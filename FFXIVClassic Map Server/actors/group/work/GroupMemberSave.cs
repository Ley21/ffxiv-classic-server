﻿namespace FFXIVClassic_Map_Server.actors.group.Work
{
    class GroupMemberSave
    {
        //For LS
        public byte rank;

        //For Retainers
        public byte cdIDOffset;
        public ushort placeName;
        public byte conditions;
        public byte level;
    }
}
