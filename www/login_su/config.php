<?php
$env_db_server  = getenv("DATABASE_SERVER");
$db_server		= ($env_db_server != false) ? $env_db_server : "localhost";
$db_username	= "root";
$db_password	= "";
$db_database	= "ffxiv_server";

$recaptcha_publickey = "";
$recaptcha_privatekey = "";

if(!defined('FFXIV_SESSION_LENGTH')) define('FFXIV_SESSION_LENGTH', 24);		//Session length in hours

?>
